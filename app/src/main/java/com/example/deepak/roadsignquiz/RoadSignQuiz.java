package com.example.deepak.roadsignquiz;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;

public class RoadSignQuiz extends AppCompatActivity {
    // String used when logging error messages
    private static final String TAG = "RoadSignQuizGame Activity";

    private List<String> fileNameList; // sign file names
    private List<String> quizSignsList; // names of signs in quiz

    private String correctAnswer; // correct sign name for the current sign
    private int totalGuesses; // number of guesses made
    private int correctAnswers; // number of correct guesses
    private int guessRows; // number of rows displaying choices
    private Random random; // random number generator
    private Handler handler; // used to delay loading next sign
    private Animation shakeAnimation; // animation for incorrect guess

    private TextView answerTextView; // displays Correct! or Incorrect!
    private TextView questionNumberTextView; // shows current question #
    private ImageView signImageView; // displays a sign
    private TableLayout buttonTableLayout; // table of answer Buttons

    // create constants for each menu id
    private final int CHOICES_MENU_ID = Menu.FIRST;
    private final int RESET_MENU_ID = Menu.FIRST + 1;

    // listen for double taps
    private GestureDetector gestureDetector;


    /**
     *
     * This method loads the road sign quiz
     *
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_road_sign_quiz);
        fileNameList = new ArrayList<String>();
        quizSignsList = new ArrayList<String>();
        guessRows = 1; //default to 1 row of choices
        random = new Random();
        handler = new Handler(); // used to perform delayed operations
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.incorrect_shake);
        shakeAnimation.setRepeatCount(3); // repeat animation 3 times

        // get references to GUI items
        questionNumberTextView = (TextView) findViewById(R.id.questionNumberTextView);
        signImageView = (ImageView) findViewById(R.id.signImageView);
        buttonTableLayout = (TableLayout) findViewById(R.id.buttonTableLayout);
        answerTextView = (TextView) findViewById(R.id.answerTextView);

        // set the questionNumberTextView's text
        questionNumberTextView.setText(getResources().getString(R.string.question) + " 1 " +
                getResources().getString(R.string.of) + " 10");
        // initialize the Gesture Detector
        gestureDetector = new GestureDetector(this,gestureListener);
        resetQuiz(); // start a new Quiz

    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        // call the GestureDetector's onTouchEvent method to check douple taps
        return gestureDetector.onTouchEvent(event);
    }

    // listen for touch events sent to the Gesture Detector
    // stubs in blank method and allows to override the needed one
    GestureDetector.SimpleOnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener(){
        // called when the user double taps the screen
        @Override
        public boolean onDoubleTap(MotionEvent e){
            // fire the cannon ball
            //create url
            String urlString = getString(R.string.trafficUrl);

            //create an Intent to launch web browser
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
            startActivity(webIntent);            // the event was handeled
            return true;
        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.add(Menu.NONE,CHOICES_MENU_ID,Menu.NONE,R.string.choices);
        menu.add(Menu.NONE, RESET_MENU_ID, Menu.NONE, R.string.reset_quiz);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // switch the menu id of the user-selected option
        switch (item.getItemId())  //gets unique ID of the item
        {
            case CHOICES_MENU_ID:
                // create a list of the possible numbers of answer choices
                final String[] possibleChoices =
                        getResources().getStringArray(R.array.guessesList);

                // create a new AlertDialog Builder and set its title
                AlertDialog.Builder choicesBuilder =
                        new AlertDialog.Builder(this);
                choicesBuilder.setTitle(R.string.choices);

                // add possibleChoices's items to the Dialog and set the
                // behavior when one of the items is clicked
                choicesBuilder.setItems(R.array.guessesList,
                        new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int item)
                            {
                                // update guessRows to match the user's choice
                                guessRows = Integer.parseInt(
                                        possibleChoices[item].toString()) / 3;
                                resetQuiz(); // reset the quiz
                            } // end method onClick
                        } // end anonymous inner class
                );  // end call to setItems

                // create an AlertDialog from the Builder
                AlertDialog choicesDialog = choicesBuilder.create();
                choicesDialog.show(); // show the Dialog
                return true;

            case RESET_MENU_ID:

                AlertDialog.Builder resetBuilder =
                        new AlertDialog.Builder(this);
                resetBuilder.setTitle(R.string.reset_quiz);
                // resets quiz when user presses the "Reset Quiz" Button
                resetBuilder.setPositiveButton(R.string.reset_quiz,
                        new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int button)
                            {
                                resetQuiz(); // reset the quiz
                            } // end method onClick
                        } // end anonymous inner class
                ); // end call to method setPositiveButton

                // create a dialog from the Builder
                AlertDialog resetDialog = resetBuilder.create();
                resetDialog.show(); // display the Dialog
                return true;
        } // end switch

        return super.onOptionsItemSelected(item);
    } // end method onOptionsItemSelected

    // created a new folder "assets" inside app/src/main which contains the images.
    // to make sure folder is created on correct path, change the android view to project view.
    private void resetQuiz() {
        // use the AssetManager to get image of sign
        AssetManager assets = getAssets(); // get the app's AssetManager
        fileNameList.clear();
        try {
            String[] paths = assets.list("");
            for (String path : paths) {
                if(path.contains(".png")) {
                    fileNameList.add(path.replace(".png", ""));
                }
            }
            Toast.makeText(this,"New Game Started",Toast.LENGTH_LONG).show();

        } catch (IOException e) {
            Log.e(TAG,"Error loading image file names", e);
        }
        correctAnswers = 0; // reset it
        totalGuesses = 0; // reset it
        quizSignsList.clear();// reset it
        // add 10 random file names to list
        int  signCounter = 1;
        int numberOfSigns = fileNameList.size();
        while(signCounter <=10) {
            int randomIndex = random.nextInt(numberOfSigns);
            String fileName = fileNameList.get(randomIndex);

            if(!quizSignsList.contains(fileName)) {
                quizSignsList.add(fileName);
                ++ signCounter;
            }

        } //while ends
        loadNextSign();
    }

    /**
     *
     * This method loads the next sign
     */
    private void loadNextSign() {
        // get file name of the next sign and remove it from the list
        String nextImageName = quizSignsList.remove(0);
        correctAnswer = nextImageName; // update the correct answer

        answerTextView.setText(""); // clear answerTextView

        // display the number of the current question in the quiz
        questionNumberTextView.setText(
                getResources().getString(R.string.question) + " " +
                        (correctAnswers + 1) + " " +
                        getResources().getString(R.string.of) + " 10");

        // use AssetManager to load next image from assets folder
        AssetManager assets = getAssets(); // get app's AssetManager
        InputStream stream; // used to read in sign images

        try {
            // get an InputStream to the asset representing the next sign
            stream = assets.open(nextImageName + ".png");

            // load the asset as a Drawable and display on the signImageView
            Drawable sign = Drawable.createFromStream(stream, nextImageName);
            signImageView.setImageDrawable(sign);
        } // end try
        catch (IOException e) {
            Log.e(TAG, "Error loading " + nextImageName, e);
        } // end catch

        // clear prior answer Buttons from TableRows
        for (int row = 0; row < buttonTableLayout.getChildCount(); ++row)
            ((TableRow) buttonTableLayout.getChildAt(row)).removeAllViews();

        Collections.shuffle(fileNameList); // shuffle file names


        // put the correct answer at the end of fileNameList later will be inserted randomly into the answer Buttons
        int correct = fileNameList.indexOf(correctAnswer);
        fileNameList.add(fileNameList.remove(correct));

        // get a reference to the LayoutInflater service
        LayoutInflater inflater = (LayoutInflater) getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        // add 3, 6, or 9 answer Buttons based on the value of guessRows
        for (int row = 0; row < guessRows; row++) {
            TableRow currentTableRow = getTableRow(row); //obtains the TableRow at a specific index in the buttonTableLayout

            // place Buttons in currentTableRow
            for (int column = 0; column < 3; column++) {
                // inflate guess_button.xml to create new Button
                Button newGuessButton =
                        (Button) inflater.inflate(R.layout.guess_button, null);

                // get country name and set it as newGuessButton's text
                String fileName = fileNameList.get((row * 3) + column);
                newGuessButton.setText(fileName);

                // register answerButtonListener to respond to button clicks
                newGuessButton.setOnClickListener(guessButtonListener);
                currentTableRow.addView(newGuessButton);
            } // end for
        } // end for

        // randomly replace one Button with the correct answer
        int row = random.nextInt(guessRows); // pick random row
        int column = random.nextInt(3); // pick random column
        TableRow randomTableRow = getTableRow(row); // get the TableRow
        String signName = correctAnswer;
        ((Button)randomTableRow.getChildAt(column)).setText(signName);
    } // end method loadNextSign

    // called when the user selects an answer
    private void submitGuess(Button guessButton) {
        String guess = guessButton.getText().toString();
        // String answer = correctAnswer;
        ++totalGuesses; // increment the number of guesses the user has made

        // if the guess is correct
        if (guess.equals(correctAnswer)) {
            ++correctAnswers; // increment the number of correct answers

            // display "Correct!" in green text
            answerTextView.setText(correctAnswer + "!");
            answerTextView.setTextColor(
                    getResources().getColor(R.color.correct_answer));

            disableButtons(); // disable all answer Buttons

            // if the user has correctly identified 10 signs
            if (correctAnswers == 10)  {
                // create a new AlertDialog Builder
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle(R.string.reset_quiz); // title bar string

                // set the AlertDialog's message to display game results
                builder.setMessage(String.format("%d %s, %.02f%% %s",
                        totalGuesses, getResources().getString(R.string.guesses),
                        (1000 / (double) totalGuesses),
                        getResources().getString(R.string.correct)));

                builder.setCancelable(false);

                // add "Reset Quiz" Button
                builder.setPositiveButton(R.string.reset_quiz,
                        new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                resetQuiz();
                            } // end method onClick
                        } // end anonymous inner class
                ); // end call to setPositiveButton
                // create AlertDialog from the Builder
                AlertDialog resetDialog = builder.create();
                resetDialog.show(); // display the Dialog
            } // end if
            else // answer is correct but quiz is not over
            {
                // load the next sign after a 1-second delay
                handler.postDelayed(
                        //anonymous inner class implementing Runnable
                        new Runnable() {
                            @Override
                            public void run()
                            {
                                loadNextSign();
                            }
                        }, 1000); // 1000 milliseconds for 1-second delay
            } // end else
        } // end if
        else // guess was incorrect
        {
            // play the animation
            signImageView.startAnimation(shakeAnimation);

            // display "Incorrect!" in red
            answerTextView.setText(R.string.incorrect_answer);
            answerTextView.setTextColor(
                    getResources().getColor(R.color.incorrect_answer));
            guessButton.setEnabled(false); // disable the incorrect answer
        } // end else
    } // end method submitGuess

    // returns the specified TableRow
    private TableRow getTableRow(int row) {
        return (TableRow) buttonTableLayout.getChildAt(row);
    } // end method getTableRow
    // utility method that disables all answer Buttons
    private void disableButtons()
    {
        for (int row = 0; row < buttonTableLayout.getChildCount(); ++row)
        {
            TableRow tableRow = (TableRow) buttonTableLayout.getChildAt(row);
            for (int i = 0; i < tableRow.getChildCount(); ++i)
                tableRow.getChildAt(i).setEnabled(false);
        } // end outer for
    } // end method disableButtons

    //called when a guess Button is touched
    private View.OnClickListener guessButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            submitGuess((Button)v);// pass selected button to submit
        }
    };
}
